jQuery(document).ready(function($) {
    $(".reset").click(function() {
        $.ajax({
            url: "/site/reset",
            type: "post",
            success: function (res) {
                $("#card > .count").text('0');
                $("#card > .total").text('0');
                $("#aed").html('0 Products');
            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.log(textStatus, errorThrown);
            }
        });
    });
    $(".addTo").click(function() {
        id = $(this).parent().find(".hidden").text();
        $.ajax({
            url: "/site/card?id="+id,
            type: "post",
            success: function (res) {
                res = JSON.parse(res);
                count = res[0];
                totalPrice = res[1];
                console.log(res[2]);
                $("#aed").html("");
                p = "";
                for(var i=0; i<=res[2].length - 1; i++) {
                    if(res[2][i].name !== undefined && res[2][i].quantity !== undefined) {
                        p += "<p>"+res[2][i].name+" : "+res[2][i].quantity+"</p>";
                        $("#aed").html(p);
                    }
                }
                $("#card > .count").text(count);
                $("#card > .total").text(totalPrice);
            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.log(textStatus, errorThrown);
            }
        });
    });
});