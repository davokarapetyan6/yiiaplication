<?php
namespace frontend\controllers;

use common\models\Authors;
use Yii;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use frontend\models\ContactForm;
use common\models\Posts;
use common\models\Products;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $authors=Posts::find()->all();
        return $this->render('index',compact('authors'));
    }

    /**
     * Logs in a user.
     *
     * @return mixed
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Logs out the current user.
     *
     * @return mixed
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return mixed
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail(Yii::$app->params['adminEmail'])) {
                Yii::$app->session->setFlash('success', 'Thank you for contacting us. We will respond to you as soon as possible.');
            } else {
                Yii::$app->session->setFlash('error', 'There was an error sending email.');
            }

            return $this->refresh();
        } else {
            return $this->render('contact', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Displays about page.
     *
     * @return mixed
     */
    public function actionAbout()
    {
        $products = Products::find()->all();

        return $this->render('about',compact('products'));
    }

    /**
     * Signs user up.
     *
     * @return mixed
     */
    public function actionSignup()
    {
        $model = new SignupForm();
        if ($model->load(Yii::$app->request->post())) {
            if ($user = $model->signup()) {
                if (Yii::$app->getUser()->login($user)) {
                    return $this->goHome();
                }
            }
        }

        return $this->render('signup', [
            'model' => $model,
        ]);
    }
    public function actionCard($id) {

        $cookies2 = Yii::$app->request->cookies;
        $card = $cookies2->getValue('newcard',[]);
        if (array_key_exists($id,$card)) {
            $card[$id]++;
        } else {
            $card[$id] = 1;
        }
        $cookies = Yii::$app->response->cookies;
        $cookies->add(new \yii\web\Cookie([
            'name' => 'newcard',
            'value' => $card,
        ]));
        $totalCount = 0;
        $totalPrice = 0;
        $allProducts = [];
        $products = Products::find()->where(['IN', 'id', array_keys($card)])->all();
        foreach($products as $product) {
            $count = 0;
            $price = 0;
            $productinfo = [];
            foreach($card as $cId => $quantity) {
                if($cId == $product->id) {
                    $productinfo['name'] = $product->name;
                    $productinfo['quantity'] = $quantity;
                    $count = $count + $quantity;
                    $price += $count*$product->coast;
                    $allProducts[] = $productinfo;
                }
            }
            $totalPrice += $price;
            $totalCount += $count;
        }

//        foreach($card as $cId => $quantity) {
//            $product = Products::find()->where(['id' => $id])->one();
//            $count = 0;
//            $price = 0;
//            $productinfo = [];
//            if($cId == $product->id) {
//                $productinfo['name'] = $product->name;
//                $productinfo['quantity'] = $quantity;
//                $count = $count + $quantity;
//                $price += $count*$product->coast;
//                $allProducts[] = $productinfo;
//            }
//            $totalPrice += $price;
//            $totalCount += $count;
//        }

        $session = Yii::$app->session;
        $session->set('totalCount',$totalCount);
        $session->set('totalPrice',$totalPrice);
        $session->set('products',$allProducts);

        return json_encode([$totalCount,$totalPrice,$allProducts]);

    }
    public function actionReset() {
        $session = Yii::$app->session;
        $session->remove('totalCount');
        $session->remove('totalPrice');
        $session->remove('products');
        Yii::$app->response->cookies->remove('newcard');
    }

    /**
     * Requests password reset.
     *
     * @return mixed
     */
    public function actionRequestPasswordReset()
    {
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', 'Check your email for further instructions.');

                return $this->goHome();
            } else {
                Yii::$app->session->setFlash('error', 'Sorry, we are unable to reset password for email provided.');
            }
        }

        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);
    }

    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($token)
    {
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->session->setFlash('success', 'New password was saved.');

            return $this->goHome();
        }

        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }
}
