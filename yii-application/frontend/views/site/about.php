<?php
/* @var $this yii\web\View */
/* @var $products frontend\controllers\SiteController */
use frontend\widgets\CartWidget;
?>
<?= CartWidget::widget()?>


<div class="container">
    <div class="row">
        <?php foreach($products as $product) : ?>
            <div class="col-md-4">
                <p><?=$product->name?></p>
                <p><?=$product->coast?></p>
                <span class="hidden"><?=$product->id?></span>
                <span class="addTo" style="display:block;padding:15px;cursor:pointer;color:white;background: #7d7d7d;">Add To Card</span>
            </div>
        <?php endforeach; ?>
    </div>
</div>

