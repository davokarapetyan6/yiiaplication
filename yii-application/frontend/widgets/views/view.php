<?php
use yii\grid\GridView;
use yii\data\ActiveDataProvider;
use common\models\Authors;
use common\models\Posts;
?>

<?php
echo '<pre>';

?>
<?= GridView::widget([
    'dataProvider' => $posts,
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],
        'title',
        [
            'attribute'=>'id',
            'label'=>'Authors',

            'format'=>'text',//raw, html

            'content'=>function($data){
                return Posts::getAuthorsFor($data->id);

            }


        ],
    ],

]); ?>

