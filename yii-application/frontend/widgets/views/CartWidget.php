<?php



?>

<div id="CardWidget" >
    <div id="card" style="padding:15px;background: #7d7d7d;color:white">
        Count : <span class="count"><?php if($session->has('totalCount')) echo $session->get('totalCount'); else echo '0';?></span>
        Price : <span class="total"><?php if($session->has('totalPrice')) echo $session->get('totalPrice'); else echo '0';?></span>
        <span class="reset" style="display:block;float:right;padding:5px;cursor:pointer; background: #000; color:white">RESET</span>
    </div>


    <div id="aed">
        <?php if($session->has('products')) : ?>
            <?php for($i=0; $i <count($productsSession); $i++) { ?>
                <p class="total"><?php if(isset($productsSession[$i]['name'])) echo $productsSession[$i]['name']." : "; ?>  <?php if(isset($productsSession[$i]['quantity'])) echo $productsSession[$i]['quantity'] ?></p>
            <?php } ?>
        <?php else : ?>
            0 Products
        <?php endif; ?>
    </div>
</div>

<?php
$this->registerJsFile('@web/js/ajaj.js',
    ['depends' => [\yii\web\JqueryAsset::className()]
    ]);
?>