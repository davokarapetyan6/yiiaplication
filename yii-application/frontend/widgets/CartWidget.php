<?php

namespace frontend\widgets;

use Yii;
use yii\base\Widget;
use yii\helpers\Html;
use common\models\Posts;
use yii\data\ActiveDataProvider;
use common\models\Products;

class CartWidget extends Widget {

    public function run()
    {
        $session = Yii::$app->session;
        $productsSession = $session->get('products');

        return $this->render('CartWidget', compact('products','session','productsSession'));
    }
}




