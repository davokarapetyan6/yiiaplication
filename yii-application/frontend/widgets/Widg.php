<?php

namespace frontend\widgets;

use yii\base\Widget;
use yii\helpers\Html;
use common\models\Posts;
use yii\data\ActiveDataProvider;

class Widg extends Widget {
    public $posts_id;

    public function run()
    {
        if(!$this->posts_id) {
            $query = Posts::find();
        } else {
            $query = Posts::find()->where(['id'=>$this->posts_id]);
        }
        $posts = new ActiveDataProvider([
            'query' => $query,
        ]);

        return $this->render('view', ['posts'=>$posts]);
    }
}

?>


