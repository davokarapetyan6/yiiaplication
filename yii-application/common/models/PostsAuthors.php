<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "posts_authors".
 *
 * @property integer $id
 * @property integer $news_id
 * @property integer $author_id
 */
class PostsAuthors extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'posts_authors';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['news_id', 'author_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */


    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'news_id' => 'News ID',
            'author_id' => 'Author ID',
        ];
    }
}
