<?php

namespace common\models;

use common\models\PostsAuthors;
use common\models\Authors;
use Yii;

/**
 * This is the model class for table "posts".
 *
 * @property integer $id
 * @property string $title
 */
class Posts extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public $authorsIds;


    public static function getNames($id) {
        $author = Authors::findOne(['id' => $id]);
        return $author->name;
    }
    public static function getAuthorsFor($id=3)  {
        $authors = PostsAuthors::find()->where(['news_id' => $id])->all();
        $names = [];
        $p = '';
        foreach($authors as $author) {
            $names[] = self::getNames($author->author_id);

        }


        for($i=0;$i<count($names);$i++) {
            $p .= '<p>'.$names[$i].'</p>';
        }
        return $p;
    }
    public static function tableName()
    {
        return 'posts';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title'], 'string'],
        ];
    }

    public function getPostAuthors()
    {
        return $this->hasMany(PostsAuthors::className(), ['news_id' => 'id']);
    }

    public function getAuthors()
    {
        return $this->hasMany(Authors::className(), ['id' => 'author_id'])
            ->via('postAuthors');
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
        ];
    }
}
