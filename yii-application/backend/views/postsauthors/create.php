<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\PostsAuthors */

$this->title = 'Create Posts Authors';
$this->params['breadcrumbs'][] = ['label' => 'Posts Authors', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="posts-authors-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
