<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\PostsAuthors */

$this->title = 'Update Posts Authors: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Posts Authors', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="posts-authors-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
