<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Posts */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="posts-form">

    <?php $form = ActiveForm::begin(['id'=>'form-signup']); ?>

    <?= $form->field($model, 'title')->textarea(['rows' => 6]) ?>
    <?php $model->isNewRecord ? $model->authorsIds = null : $model->authorsIds = $checkedAuthors;  ?>
    <?= $form->field($model,'authorsIds')->checkboxList($authorsList) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary','name'=>'signup']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
