<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\PostsAuthors */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="posts-authors-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'news_id')->dropDownList($posts) ?>

    <?= $form->field($model, 'author_id')->dropDownList($authors) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
