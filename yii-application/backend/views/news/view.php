<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $model backend\models\News */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'News', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="news-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'title:ntext',
            'author_id',
        ],
    ]) ?>
<?php
$authors = new \yii\data\ActiveDataProvider([
    'query' => $model->postAuthors
]);
print($model->title);die;

?>
    <?= GridView::widget([
        'dataProvider' => $authors,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'title',
            [
                'attribute'=>'id',
                'label'=>'Authors',

                'format'=>'text',//raw, html

                'content'=>function($data){
                    return Posts::getAuthorsFor($data->id);

                }


            ],
        ],

    ]);?>

</div>
