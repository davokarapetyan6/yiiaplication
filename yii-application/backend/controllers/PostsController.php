<?php

namespace backend\controllers;

use common\models\Authors;
use common\models\PostsAuthors;
use Yii;
use common\models\Posts;
use common\models\PostsSearch;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * PostsController implements the CRUD actions for Posts model.
 */
class PostsController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Posts models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PostsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Posts model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        $post_views = $model->post_views;
        $session = Yii::$app->session;
        $pageViews = $session->get('pageViews', []);



        if(!in_array($id, $pageViews)) {
            $pageViews[] = $id;
            $session->set('pageViews', $pageViews);
            $post_views++;
            $model->post_views = $post_views;
            $model->update();
        }

       /* if(!$session->has('pages')) {
            $session->set('pages',[]);
        }
        $add = true;
        $visitedPages = $session->get('pages');
        if(count($visitedPages) == 0) {
            $add = true;
        } else {
            foreach($visitedPages as $pageId) {
                if($pageId == $id) {
                    $add = false;
                }
            }
        }
        if($add) {
            array_push($visitedPages,$id);
            $session->set('pages',$visitedPages);
        }
        foreach($session->get('pages') as $page) {
            if($page == $id) {
                $post_views++;
                $model = Posts::findOne($id);
                $model->post_views = $post_views;
                $model->update();
            }
        }*/
        return $this->render('view', [
            'model' => $model
        ]);
    }

    /**
     * Creates a new Posts model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Posts();
        $authorsList = ArrayHelper::map(Authors::find()->all(),'id','name');
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $post = Yii::$app->request->post('Posts');
            $authorsIds = $post['authorsIds'];
            if($authorsIds) {
                foreach($authorsIds as $authorId) {
                    $postauthors = new PostsAuthors();
                    $postauthors->news_id = $model->id;
                    $postauthors->author_id = $authorId;
                    $postauthors->save();
                }
            }
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
                'authorsList' => $authorsList
            ]);
        }
    }

    /**
     * Updates an existing Posts model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $authorsList = ArrayHelper::map(Authors::find()->all(),'id','name');
        $autsch = $model->postAuthors;
        $checkedAuthors = [];
        foreach($autsch as $one) {
            array_push($checkedAuthors,$one->author_id);
        }
        if ($model->load(Yii::$app->request->post()) && $model->save()) {

            $post = Yii::$app->request->post('Posts');
            $authorsIds = $post['authorsIds'];
           PostsAuthors::deleteAll(['news_id' => $model->id]);
            if($authorsIds) {
                foreach($authorsIds as $authorId) {
                    $postauthors = new PostsAuthors();
                    $postauthors->news_id = $model->id;
                    $postauthors->author_id = $authorId;
                    $postauthors->save();
                }
            }

            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'authorsList' => $authorsList,
                'checkedAuthors' => $checkedAuthors
            ]);
        }
    }

    /**
     * Deletes an existing Posts model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Posts model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Posts the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Posts::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
